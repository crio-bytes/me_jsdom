setupDaysMap = function () {
    var numDays = new Map();
    numDays.set("01", "31");
    numDays.set("02", "28");
    numDays.set("03", "31");
    numDays.set("04", "30");
    numDays.set("05", "31");
    numDays.set("06", "30");
    numDays.set("07", "31");
    numDays.set("08", "31");
    numDays.set("09", "30");
    numDays.set("10", "31");
    numDays.set("11", "30");
    numDays.set("12", "31");
    return numDays;
}

fetchTiingoData = function (month, year) {
    numDays = setupDaysMap();
    let startDate = new Date(year + "-" + month + "-" + "01");
    let endDate = new Date(year + "-" + month + "-" + numDays.get(month));
    var filteredData = stockData.filter(candle => new Date(candle.date) >= startDate && new Date(candle.date) <= endDate)
    return filteredData;
}

repaintTableDiv = function(month, year) {
    // TODO - replace the below code with correct method call.
    let filteredData = fetchTiingoData(month, year);
    document.getElementById("tablediv").innerText = JSON.stringify(filteredData);
}

function createBasicTable(filteredData) {
    // TODO Repaint the tableDiv based on filteredDate
}

function addtr(tbody, candle) {
    // TODO You can use this method to paint one row
}

function addHeading(thead, table) {
    let theadTr = document.createElement('tr');
    addTd('Date', theadTr);
    addTd("Open", theadTr);
    addTd("Close", theadTr);
    addTd("High", theadTr);
    addTd("Low", theadTr);
    addTd("Volume", theadTr);
    thead.append(theadTr);
    table.append(thead);
}

function addTd(content, tr) {
    let td = document.createElement('td');
    td.innerHTML = content;
    tr.append(td);
}


attachEvents = function() {
    // TODO: We have two select boxes, for month and year.
    //  When any of these dropdown changes its value, the contents of #tableDiv needs to be repainted.
    //  hint: check document.getElementById function.
}

repaintTableDiv('02', '2020')
