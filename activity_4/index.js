function addTd(id, tr) {
    let col = document.createElement('td');
    col.innerHTML = id;
    tr.append(col);
}

function createBasicTable() {
    let table = document.createElement("table");
    table.setAttribute('id', 'studentsTable')
    return table;
}

function addTr(thead) {
    let theadTr = document.createElement('tr');
    thead.append(theadTr);
    return theadTr;
}

// task is to append simple "hello world" in the body
function loadTable() {
    let table = createBasicTable();
}
