setupDaysMap = function () {
    var numDays = new Map();
    numDays.set("01", "31");
    numDays.set("02", "28");
    numDays.set("03", "31");
    numDays.set("04", "30");
    numDays.set("05", "31");
    numDays.set("06", "30");
    numDays.set("07", "31");
    numDays.set("08", "31");
    numDays.set("09", "30");
    numDays.set("10", "31");
    numDays.set("11", "30");
    numDays.set("12", "31");
    return numDays;
}

fetchTiingoData = function (month, year) {
    numDays = setupDaysMap();
    let startDate = new Date(year + "-" + month + "-" + "01");
    let endDate = new Date(year + "-" + month + "-" + numDays.get(month));
    var filteredData = stockData.filter(candle => new Date(candle.date) >= startDate && new Date(candle.date) <= endDate)
    return filteredData;
}

repaintTableDiv = function(month, year) {
    // document.getElementById("tablediv").innerText = JSON.stringify(fetchTiingoData(month, year));
    // Actual code
    let table = createBasicTable(fetchTiingoData(month, year));
}

function createBasicTable(filteredData) {
    document.getElementById("tablediv").innerHTML = '';
    let table = document.createElement("table");
    table.setAttribute('id', 'tiingoTable')
    table.setAttribute('class', 'table');
    let thead = document.createElement('thead');
    addHeading(thead, table);
    let tbody = document.createElement('tbody');
    table.append(tbody)
    filteredData.forEach(candle => {
        addtr(tbody, candle);
    });
    document.getElementById("tablediv").append(table);
}

function addtr(tbody, candle) {
    let tr = document.createElement('tr');
    addTd(candle.date, tr);
    addTd(candle.open, tr);
    addTd(candle.close, tr);
    addTd(candle.high, tr);
    addTd(candle.low, tr);
    addTd(candle.volume, tr);
    tbody.append(tr);
}

function addHeading(thead, table) {
    let theadTr = document.createElement('tr');
    addTd('Date', theadTr);
    addTd("Open", theadTr);
    addTd("Close", theadTr);
    addTd("High", theadTr);
    addTd("Low", theadTr);
    addTd("Volume", theadTr);
    thead.append(theadTr);
    table.append(thead);
}

function addTd(content, tr) {
    let td = document.createElement('td');
    td.innerHTML = content;
    tr.append(td);
}

handleDropdownChange = function(event) {
    let yearVal = document.getElementById('year').value;
    let monthVal = document.getElementById('month').value;
    repaintTableDiv(monthVal, yearVal);
}

attachEvents = function() {
    document.getElementById('year').addEventListener('change', handleDropdownChange)
    document.getElementById('month').addEventListener('change', handleDropdownChange)
}

repaintTableDiv('02', '2020')
attachEvents();
