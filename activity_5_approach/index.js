function addTd(id, tr) {
    let col = document.createElement('td');
    col.innerHTML = id;
    tr.append(col);
}

function createBasicTable() {
    let table = document.createElement("table");
    table.setAttribute('id', 'studentsTable')
    return table;
}

function addTr(thead) {
    let theadTr = document.createElement('tr');
    thead.append(theadTr);
    return theadTr;
}

// task is to append simple "hello world" in the body
function loadTable() {
    let table = createBasicTable();
    let thead = document.createElement('thead');
    let refTr = addTr(thead);
    addTd('ID', refTr);
    addTd("Name", refTr);
    table.append(thead)

    let tbody = document.createElement('tbody');
    table.append(tbody)

    // TOTO Replace the console logs below and add code to create the table with 5 rows.
    console.log(students.length);
    students.forEach(value => {
        console.log(value.id + ", " + value.name)
    });

    document.getElementById("tablediv").append(document.createElement('hr'));
    document.getElementById("tablediv").append(table);
}

setButtonClass = function() {
    // TODO - change the style of the students table to table-danger
    document.getElementById('loadButton').className = 'btn btn-danger';
}

attachEvents = function() {
    document.getElementById('styleButton').addEventListener('click', setButtonClass);
}

attachEvents();
