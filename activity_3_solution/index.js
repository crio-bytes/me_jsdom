function loadTable() {
    let table = document.createElement("table");
    table.setAttribute('border', 'solid')
    table.setAttribute('width', '200px')
    let thead = document.createElement('thead');
    let theadTr = document.createElement('tr');
    let idCol = document.createElement('td');
    idCol.innerHTML = 'ID';
    let nameCol = document.createElement('td');
    nameCol.innerHTML = 'Name'
    theadTr.append(idCol, nameCol);
    thead.append(theadTr);
    table.append(thead)

    let tbody = document.createElement('tbody');
    table.append(tbody)

    // TODO: refer to above code and try adding more data in the table/
    document.getElementById("tablediv").append(document.createElement('hr'));
    document.getElementById("tablediv").append(table);
}
